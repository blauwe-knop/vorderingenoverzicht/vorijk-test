// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package test_helpers

type KeyPair struct {
	PublicKey  string
	PrivateKey string
}
