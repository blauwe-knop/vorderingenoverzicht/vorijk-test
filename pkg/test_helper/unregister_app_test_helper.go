// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package test_helpers

import (
	sessionModel "gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"

	repository_log_wrappers "gitlab.com/blauwe-knop/vorderingenoverzicht/vorijk-test/pkg/repository_log_wrapper"
)

func StartSession(
	cryptographyTestHelper *CryptographyTestHelper,
	sessionTestHelper *repository_log_wrappers.SessionTestHelper,
	appPublicKey string,
	appPrivateKey string,
	organizationPublicKey string,
) (string, error) {
	challengeRequestMessage := sessionModel.ChallengeRequestMessage{
		AppPublicKey: appPublicKey,
	}

	encryptedChallengeRequestMessage, err := cryptographyTestHelper.EncryptChallengeRequestMessage(
		challengeRequestMessage,
		organizationPublicKey,
	)
	if err != nil {
		return "", err
	}

	challengeRequest := sessionModel.ChallengeRequest{
		Request: encryptedChallengeRequestMessage,
	}

	challenge, err := sessionTestHelper.CreateChallenge(
		challengeRequest,
	)
	if err != nil {
		return "", err
	}

	err = cryptographyTestHelper.VerifyNonce(
		*challenge,
		organizationPublicKey,
	)
	if err != nil {
		return "", err
	}

	signature, err := cryptographyTestHelper.SignNonce(
		challenge.Nonce,
		appPrivateKey,
	)
	if err != nil {
		return "", err
	}

	challengeResult := sessionModel.ChallengeResult{
		AppSignature: signature,
	}

	encryptedChallengeResult, err := cryptographyTestHelper.EncryptChallengeResult(
		challengeResult,
		organizationPublicKey,
	)
	if err != nil {
		return "", err
	}

	challengeResponse := sessionModel.ChallengeResponse{
		Nonce:    challenge.Nonce,
		Response: encryptedChallengeResult,
	}

	session, err := sessionTestHelper.CompleteChallenge(
		challengeResponse,
	)
	if err != nil {
		return "", err
	}

	return session.Token, nil
}
