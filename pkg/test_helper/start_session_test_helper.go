// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package test_helpers

import (
	appManagementProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"

	repository_log_wrappers "gitlab.com/blauwe-knop/vorderingenoverzicht/vorijk-test/pkg/repository_log_wrapper"
)

func UnregisterApp(
	appManagementRepositoryLogWrapper *repository_log_wrappers.AppManagementRepositoryLogWrapper,
	cryptographyTestHelper *CryptographyTestHelper,
	appManagementSessionTestHelper *repository_log_wrappers.SessionTestHelper,
	appKeyPair *KeyPair,
	appManagerPublicKey string,
	registrationToken string,
) error {
	signature, err := cryptographyTestHelper.SignRegistrationToken(
		registrationToken,
		appKeyPair.PrivateKey,
	)
	if err != nil {
		return err
	}

	appManagerSessionToken, err := StartSession(
		cryptographyTestHelper,
		appManagementSessionTestHelper,
		appKeyPair.PublicKey,
		appKeyPair.PrivateKey,
		appManagerPublicKey,
	)
	if err != nil {
		return err
	}

	unregisterAppRequest := appManagementProcessModel.UnregisterAppRequest{
		RegistrationToken: registrationToken,
		Signature:         signature,
	}

	err = appManagementRepositoryLogWrapper.UnregisterApp(
		appManagerSessionToken,
		unregisterAppRequest,
	)
	if err != nil {
		return err
	}

	return err
}
