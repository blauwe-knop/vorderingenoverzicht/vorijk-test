// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repository_log_wrappers

import (
	"fmt"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	appManagementProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/model"
	appManagementRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process/pkg/repositories"
	appManagementServiceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service/pkg/model"
	"go.uber.org/zap"
)

type AppManagementRepositoryLogWrapper struct {
	Logger                         *zap.Logger
	AppManagementProcessRepository appManagementRepositories.AppManagementProcessRepository
}

func NewAppManagementRepositoryLogWrapper(logger *zap.Logger, registrationApiUrl string) *AppManagementRepositoryLogWrapper {
	logger.Debug("create appManagementProcessRepository", zap.String("registrationApiUrl", registrationApiUrl))
	appManagementProcessRepository := appManagementRepositories.NewAppManagementProcessClient(
		registrationApiUrl,
	)

	return &AppManagementRepositoryLogWrapper{
		Logger:                         logger,
		AppManagementProcessRepository: appManagementProcessRepository,
	}
}

func (uc *AppManagementRepositoryLogWrapper) RegisterApp(appManagerSessionToken string, registerAppRequest appManagementProcessModel.RegisterAppRequest) (*appManagementProcessModel.RegisterAppResponse, error) {
	uc.Logger.Debug("appManagementProcessRepository.RegisterApp",
		zap.String("appManagerSessionToken", appManagerSessionToken),
		zap.Reflect("registerAppRequest", registerAppRequest),
	)
	registerAppResponse, err := uc.AppManagementProcessRepository.RegisterApp(
		appManagerSessionToken,
		registerAppRequest,
	)
	if err != nil {
		uc.Logger.Error("appManagementProcessRepository.RegisterApp failed", zap.Error(err))
		return nil, fmt.Errorf("appManagementProcessRepository.RegisterApp failed: %v", err)
	}

	uc.Logger.Info("appManagementProcessRepository.RegisterApp succeeded", zap.Reflect("registerAppResponse", registerAppResponse))

	return registerAppResponse, nil
}

func (uc *AppManagementRepositoryLogWrapper) LinkUserIdentity(registrationToken string, userIdentity appManagementProcessModel.UserIdentity) (*appManagementServiceModel.Registration, error) {
	uc.Logger.Debug("appManagementProcessRepository.LinkUserIdentity",
		zap.String("registrationToken", registrationToken),
		zap.Reflect("userIdentity", userIdentity),
	)
	registration, err := uc.AppManagementProcessRepository.LinkUserIdentity(
		registrationToken,
		userIdentity,
	)
	if err != nil {
		uc.Logger.Error("appManagementProcessRepository.LinkUserIdentity failed", zap.Error(err))
		return nil, fmt.Errorf("appManagementProcessRepository.LinkUserIdentity failed: %v", err)
	}

	uc.Logger.Info("AppManagementProcessRepository.LinkUserIdentity succeeded", zap.Reflect("registration", registration))

	return registration, err
}

func (uc *AppManagementRepositoryLogWrapper) FetchCertificate(sessionToken string, registrationToken string) (*appManagementProcessModel.FetchCertificateResponse, error) {
	uc.Logger.Debug("AppManagementProcessRepository.FetchCertificate",
		zap.String("sessionToken", sessionToken),
		zap.String("registrationToken", registrationToken),
	)
	fetchCertificateResponse, err := uc.AppManagementProcessRepository.FetchCertificate(
		sessionToken,
		registrationToken,
	)
	if err != nil {
		uc.Logger.Error("AppManagementProcessRepository.FetchCertificate failed", zap.Error(err))
		return nil, fmt.Errorf("AppManagementProcessRepository.FetchCertificate failed: %v", err)
	}

	uc.Logger.Info("AppManagementProcessRepository.FetchCertificate succeeded", zap.Reflect("fetchCertificateResponse", fetchCertificateResponse))

	return fetchCertificateResponse, nil
}

func (uc *AppManagementRepositoryLogWrapper) UnregisterApp(appManagerSessionToken string, unregisterAppRequest appManagementProcessModel.UnregisterAppRequest) error {
	uc.Logger.Debug("AppManagementProcessRepository.UnregisterApp",
		zap.String("appManagerSessionToken", appManagerSessionToken),
		zap.Reflect("unregisterAppRequest", unregisterAppRequest),
	)
	err := uc.AppManagementProcessRepository.UnregisterApp(
		appManagerSessionToken,
		unregisterAppRequest,
	)
	if err != nil {
		uc.Logger.Error("AppManagementProcessRepository.UnregisterApp failed", zap.Error(err))
		return fmt.Errorf("AppManagementProcessRepository.UnregisterApp failed: %v", err)
	}

	uc.Logger.Debug("AppManagementProcessRepository.UnregisterApp succeeded")

	return nil
}

func (uc *AppManagementRepositoryLogWrapper) CheckRegistrationApiHealth() error {
	panic("CheckRegistrationApiHealth not implemented")

	// 	uc.Logger.Debug("appManagementProcessRepository.GetHealth")
	// 	err := uc.AppManagementProcessRepository.GetHealth()
	// 	if err != nil {
	// 		uc.Logger.Error("app management process - offline", zap.Error(err))
	// 		return fmt.Errorf("app management process - offline: %v", err)
	// 	}

	// 	uc.Logger.Info("AppManagementProcessRepository.GetHealth succeeded")

	// return nil
}

func (uc *AppManagementRepositoryLogWrapper) CheckRegistrationApiHealthCheck() (healthcheck.Result, error) {
	uc.Logger.Debug("AppManagementProcessRepository.GetHealthCheck")
	healthCheckResult := uc.AppManagementProcessRepository.GetHealthCheck()
	if healthCheckResult.Status == healthcheck.StatusError {
		uc.Logger.Error("AppManagementProcessRepository.GetHealthCheck failed", zap.Reflect("healthCheckResult", healthCheckResult))
		return healthCheckResult, fmt.Errorf("AppManagementProcessRepository.GetHealthCheck failed: %v", healthCheckResult)
	}

	uc.Logger.Info("AppManagementProcessRepository.GetHealthCheck succeeded", zap.Reflect("healthCheckResult", healthCheckResult))

	return healthCheckResult, nil
}
