// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repository_log_wrappers

import (
	"fmt"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	citizenFinancialClaimModel "gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/model"
	citizenFinancialClaimRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process/pkg/repositories"
	"go.uber.org/zap"
)

type CitizenFinancialClaimRepositoryLogWrapper struct {
	CitizenFinancialClaimRepository citizenFinancialClaimRepositories.CitizenFinancialClaimRepository
	Logger                          *zap.Logger
}

func NewCitizenFinancialClaimRepositoryLogWrapper(logger *zap.Logger, financialClaimRequestApiUrl string) *CitizenFinancialClaimRepositoryLogWrapper {
	logger.Debug("create citizenFinancialClaimRepository", zap.String("financialClaimRequestApiUrl", financialClaimRequestApiUrl))
	citizenFinancialClaimRepository := citizenFinancialClaimRepositories.NewCitizenFinancialClaimClient(
		financialClaimRequestApiUrl,
	)

	return &CitizenFinancialClaimRepositoryLogWrapper{
		Logger:                          logger,
		CitizenFinancialClaimRepository: citizenFinancialClaimRepository,
	}
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) ConfigureClaimsRequest(sessionToken string, configurationRequest citizenFinancialClaimModel.ConfigurationRequest) (*citizenFinancialClaimModel.Configuration, error) {
	uc.Logger.Debug("CitizenFinancialClaimRepository.ConfigureClaimsRequest",
		zap.String("sessionToken", sessionToken),
		zap.Reflect("configurationRequest", configurationRequest),
	)
	configuration, err := uc.CitizenFinancialClaimRepository.ConfigureClaimsRequest(
		sessionToken,
		configurationRequest,
	)
	if err != nil {
		uc.Logger.Error("CitizenFinancialClaimRepository.ConfigureClaimsRequest failed", zap.Error(err))
		return nil, fmt.Errorf("CitizenFinancialClaimRepository.ConfigureClaimsRequest failed: %v", err)
	}

	uc.Logger.Info("CitizenFinancialClaimRepository.ConfigureClaimsRequest succeeded", zap.Reflect("configuration", configuration))

	return configuration, nil
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) RequestFinancialClaimsInformation(sessionToken string, configurationToken string) (*citizenFinancialClaimModel.FinancialClaimsInformation, error) {
	uc.Logger.Debug("CitizenFinancialClaimRepository.RequestFinancialClaimsInformation",
		zap.String("sessionToken", sessionToken),
		zap.Reflect("configurationToken", configurationToken),
	)
	financialClaimsInformation, err := uc.CitizenFinancialClaimRepository.RequestFinancialClaimsInformation(
		sessionToken,
		configurationToken,
	)
	if err != nil {
		uc.Logger.Error("CitizenFinancialClaimRepository.RequestFinancialClaimsInformation failed", zap.Error(err))
		return nil, fmt.Errorf("CitizenFinancialClaimRepository.RequestFinancialClaimsInformation failed: %v", err)
	}

	uc.Logger.Info("CitizenFinancialClaimRepository.RequestFinancialClaimsInformation succeeded", zap.Reflect("financialClaimsInformation", financialClaimsInformation))

	return financialClaimsInformation, nil
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) CheckFinancialClaimRequestApiHealth() error {
	uc.Logger.Debug("citizenFinancialClaimRepository.GetHealth")
	err := uc.CitizenFinancialClaimRepository.GetHealth()
	if err != nil {
		uc.Logger.Error("citizenFinancialClaimRepository.GetHealth failed", zap.Error(err))
		return fmt.Errorf("citizenFinancialClaimRepository.GetHealth failed: %v", err)
	}

	uc.Logger.Info("citizen financial claim process - online")

	return nil
}

func (uc *CitizenFinancialClaimRepositoryLogWrapper) CheckFinancialClaimRequestApiHealthCheck() (healthcheck.Result, error) {
	uc.Logger.Debug("CitizenFinancialClaimRepository.GetHealthCheck")
	healthCheckResult := uc.CitizenFinancialClaimRepository.GetHealthCheck()
	if healthCheckResult.Status == healthcheck.StatusError {
		uc.Logger.Error("CitizenFinancialClaimRepository.GetHealthCheck failed", zap.Reflect("healthCheckResult", healthCheckResult))
		return healthCheckResult, fmt.Errorf("CitizenFinancialClaimRepository.GetHealthCheck failed: %v", healthCheckResult)
	}

	uc.Logger.Info("CitizenFinancialClaimRepository.GetHealthCheck succeeded", zap.Reflect("healthCheckResult", healthCheckResult))

	return healthCheckResult, nil
}
