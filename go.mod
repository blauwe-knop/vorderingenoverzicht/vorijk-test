module gitlab.com/blauwe-knop/vorderingenoverzicht/vorijk-test

go 1.21.4

require (
	gitlab.com/blauwe-knop/common/bk-crypto v0.0.0-20240225142119-35276c9754be
	gitlab.com/blauwe-knop/common/health-checker v0.0.4
	gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-process v0.16.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/app-management-service v0.16.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/citizen-financial-claim-process v0.16.1
	gitlab.com/blauwe-knop/vorderingenoverzicht/mock-source-system v0.16.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process v0.16.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/service-discovery-process v0.15.2
	gitlab.com/blauwe-knop/vorderingenoverzicht/session-process v0.16.0
	go.uber.org/zap v1.27.0
)

require (
	github.com/btcsuite/btcd/btcec/v2 v2.3.2 // indirect
	github.com/ethereum/go-ethereum v1.13.13 // indirect
	github.com/holiman/uint256 v1.2.4 // indirect
	golang.org/x/sys v0.17.0 // indirect
)

require (
	github.com/decred/dcrd/dcrec/secp256k1/v4 v4.2.0 // indirect
	github.com/golang-jwt/jwt/v5 v5.2.0
	gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-service v0.16.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	golang.org/x/crypto v0.20.0 // indirect
)
